# Comment contribuer ?

Merci de prendre le temps de faire évoluer ce site web ! 🎉

Le code de ce site web repose sur Git pour le contrôle de version et est hébergé
sur GitLab.com.

Le code mis en production est celui de la dernière branche taggée (par exemple v1.5.0).
La branche `develop` est la branche de développement 

## Soumettre des changements

Vous avez trouvé un bug ou voulez proposer une nouvelle fonctionnalité ?

### Vous n'avez pas les droits d'écriture sur le dépôt de l'incubateur (cas le plus probable)

- Commencez par dupliquer (fork) le [dépôt de l'incubateur](https://gitlab.com/incubateur-territoires/incubateur/website-incubateur).
- Créez vos changements dans une branche dédiée de votre dépôt local.
- Créez une merge request via GitLab. La branche source est votre branche locale et la branche cible est
la branche `develop` du dépôt de l'incubateur.

Un·e développeur·euse de l'équipe commentera et/ou approuvera votre requête et fera
un merge de vos changements définitifs sur la branche `develop` du dépôt.

### Vous avez les droits en écriture sur le dépôt de l'incubateur

- Commencez par cloner le [dépôt de l'incubateur](https://gitlab.com/incubateur-territoires/incubateur/website-incubateur).
- Créez vos changements localement dans une branche dédiée.
- Pushez votre branche locale vers le dépôt distant.
- Créez une merge request et assignez un·e developpeur·euse en tant que reviewer.
- Une fois votre requête approuvée, vous pouvez la merger.

### Conventions

- Les prefixes `feat/` et `fix/` sont utilisés pour nommer les branches (par exemple : `fix/minor-ui-fixes).
- De la même façon les messages de commit sont précédés par `fix:` ou `feat:`(par exemple : `feat: add feedback-widget button`).

## Style du code

Nous utilisons un [linter](https://mindsers.blog/fr/post/linting-good-practices/) avec les règles suivantes :

- Le guide de style d'[Airbnb](https://github.com/nmussy/javascript-style-guide) pour Javascript.
- Les conventions officielles pour du code Vue ([plugin:vue/recommended](https://fr.vuejs.org/v2/style-guide/index.html)).
- Les règles recommandées de la communauté de [Nuxt](https://github.com/nuxt/eslint-config).

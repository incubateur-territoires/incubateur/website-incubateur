import qs from 'qs';
import { INDEX } from '~/plugins/meilisearch';
import searchService from '~/pages/services/index';

const SCALE_FILTER = ['Communes', 'Départements', 'Régions', 'EPCI'];

const THEME_FILTER = [
  'Transition écologique',
  'Équipements et patrimoine',
  'Développement économique local',
  'Solidarités et inclusion numérique',
  'Mobilités',
  'Outils des élus et des agents',
  'Relation et communication avec les usagers',
  'Education et Enfance',
  "Accès à l'emploi",
  'Données publiques',
  'Dématérialisation',
  'Finances publiques',
  'Démocratie',
  'Gestion des risques',
  'Gestion des interventions',
  'Déchets',
  'Cartographie',
  'Assemblée délibérante à distance',
  'Bureautique',
  'Sobriété numérique',
  'Site web',
  'Commande publique',
  'Signature éléctronique',
  'Faciliter les démarches des habitants',
  'Simulateur',
].sort();

const PROGRAM_OPTIONS = [
  "Startup d'Etat ou de territoires",
  'Service lauréat FR',
  'Tous les programmes',
];

export default () => ({
  data() {
    return {
      SCALE_FILTER,
      THEME_FILTER,
      PROGRAM_OPTIONS,
    };
  },
  methods: {
    async filterServices() {
      const filter = this.generateFilter();
      const queryFilter = qs.stringify(filter);

      await this.$router.replace({
        path: this.$route.path,
        query: {
          ...this.$route.query,
          filter: queryFilter,
          search: this.form.searchString,
        },
      });

      const { services, total } = await searchService(this.$search, {
        index: INDEX,
        pagination: this.pagination,
        filter,
        searchString: this.form.searchString,
      });

      this.services = services;
      this.setPagination({ total, current: 1 });
    },
    generateFilter() {
      return [
        this.generateScalesFilter(),
        this.generateThemesFilter(),
        this.generateProgramFilter(),
        this.generateTypesFilter(),
        this.generateStatusFilter(),
      ].filter(({ length }) => length > 0);
    },
    generateScalesFilter() {
      const filter = this.form.scales.map((scale) => `target = "${scale}"`);

      return filter.length ? filter : '';
    },
    generateThemesFilter() {
      return this.form.themes.map(
        (theme) => `themes = "${theme}" OR competence = "${theme}"`,
      );
    },
    generateProgramFilter() {
      return this.form.program && this.form.program !== 'Tous les programmes'
        ? `program = "${this.form.program}"`
        : '';
    },
    generateStatusFilter() {
      return this.form.isAvailable ? `stage = "Disponible"` : '';
    },
    generateTypesFilter() {
      const filter = [];

      if (this.form.isService) {
        filter.push('type = "Service numérique"');
      }

      if (this.form.isGuide) {
        filter.push('type = "Kit et Guide"');
      }

      if (this.form.isANSM) {
        filter.push('type = "ANSM"');
      }

      return filter.length ? filter : '';
    },
  },
});

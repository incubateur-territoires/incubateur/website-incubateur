# Incubateur des Territoires

Le site public de l'Incubateur des Territoires.

## Technologies mises en œuvre
Le frontend est en Vue.js 2 avec [Nuxt](https://nuxtjs.org/fr/) comme framework.

Le contenu est géré grâce au CMS Headless [Directus](https://docs.directus.io/) (plus d'informations sur
les CMS Headless [ici](https://www.lafabriquedunet.fr/blog/headless-cms/)).

Le moteur de recherche intégré à l'application est fourni par [Meilisearch](https://docs.meilisearch.com/).

## Développement local
Voici les étapes à suivre pour lancer une instance locale du site de l'Incubateur des Territoires.

### Définir les variables d'environnement
Les variables d'environnement nécessaires sont les suivantes :

- API_URL et API_TOKEN pour communiquer avec le backend Directus.
- MEILISEARCH_HOST et MEILISEARCH_API_KEY pour le service de recherche Meilisearch.

### Lancer le service
```
git clone https://gitlab.com/incubateur-territoires/incubateur/website-incubateur.git
cd website-incubateur
npm install
npm run dev
```

Le serveur de développement devrait être maintenant accessible sur `localhost:3000`.

## Cycle de vie de l'ajout de services

Les pages services ont deux logiques différentes actuellement :
- La liste des services interagit avec le moteur de recherche
- Le détail d'un service avec Directus

Les pages de détail sont automatiquement générées à la création de la donnée sur Directus et sont à jour de ce dernier.
Par contre, les filtres et la base de recherche du moteur de recherche doit être mise à jour par rapport à Directus manuellement.
Il est donc possible d'avoir un delta entre un détail de service (Directus) et les cartes/filtres de la liste des services (Meilisearch).

Si un delta est constaté, merci de créer une issue ou de contacter l'équipe projet.

## Déploiement en production

Le déploiement du site se fait via GitLab CI/CD, à l'aide de tags de commits pour gérer les versions.

### Étapes pour déployer :
1. **Créer un tag pour la version à déployer**  
   Utilisez la commande suivante pour créer un tag sur un commit avec la version de la release :  
```bash
git tag -a v1.23.2 719bdf1ff6cf4a5c31e795b14530488b6c15fb3a -m "Deploiement nouveau menu FINDPE (v1.23.2)"
```

2. **Pousser le tag dans le dépôt Git**  
   Une fois le tag créé, poussez-le dans le dépôt avec :  
```bash
git push origin v1.23.2
```

3. **Lancer le job de déploiement**  
   Après le merge du commit correspondant, un job GitLab sera disponible pour être lancé manuellement. Accédez à ce job via l'interface GitLab :  
   [Lien vers les jobs de déploiement](https://gitlab.com/incubateur-territoires/incubateur/website-incubateur/-/jobs)


